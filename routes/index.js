const express = require('express');
const router = express.Router();
const hostname = require('os').hostname();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Simple Ubuntu Xenial'
                      , hostname :  hostname
                      , utc: new Date().toUTCString()
                      })
});

module.exports = router;
